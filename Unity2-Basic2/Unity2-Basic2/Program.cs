﻿using System;
using System.Diagnostics;
using System.Text;

namespace Unity2Basic2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Study6 study6 = new Study6();
            study6.TestEnum();
            study6.TestSwitch();

            Study7 study7 = new Study7();
            study7.TestFor();
            study7.TestForEach();
            study7.TestWhile();
            study7.TestDoWhile();
            study7.TestBreakContinue();
            study7.TestGoTo();

            Study8 study8 = new Study8();
            study8.TestException();
            study8.TestThrow();

            Study9 study9 = new Study9();
            study9.TestArray();
            study9.TestArray2();
            study9.TestJaggedArray();
            study9.TestArrayCopy();
            study9.TestArraySort();

            Study10 study10 = new Study10();
            study10.TestFormat();
            study10.TestComa();
            study10.TestFullZero();
            study10.TestAbbreviation();
            study10.TestStringBuilder();
        }
    }

    class Study6 {
        public enum Country {Korea, China, America}

        public void TestEnum () {
            Country myCountry = Country.Korea;
            
            if (myCountry == Country.Korea) {
                Console.WriteLine("한국");
            } else if (myCountry == Country.China) {
                Console.WriteLine("중국");
            } else if (myCountry == Country.America) {
                Console.WriteLine("미국");
            } else {
                Console.WriteLine("선택하시오");
            }
        }

        public void TestSwitch() {
            Country myCountry = Country.Korea;

            switch (myCountry) {
                case Country.Korea:
                    Console.WriteLine("한국");
                    break;
                case Country.China:
                    Console.WriteLine("중국");
                    break;
                case Country.America:
                    Console.WriteLine("미국");
                    break;
                default:
                    Console.WriteLine("Hello??");
                    break;
            }
        }
    }

    class Study7 {
        public void TestFor() {
            int result = 0;

            for (int i = 0; i < 10; i++) {
                result += i;
            }

            Console.WriteLine("For Result :: {0}",result);
        }

        public void TestForEach() {
            int result = 0;
            int[] numbers = new int[10];

            for (int i = 0; i < 10; i++) {
                numbers[i] = i + 1;
            }

            foreach(int i in numbers) {
                result += i;
            }

            Console.WriteLine("Foreach Result :: {0}",result);
        }

        public void TestWhile() {
            int result = 0;

            int startNumber = 1;

            while (startNumber <= 10) {
                result += startNumber;
                startNumber++;
            }

            Console.WriteLine("While Result :: {0}",result);
        }

        public void TestDoWhile() {
            int result = 0;
            int startNumber = 1;

            do
            {
                result += startNumber;
                startNumber++;
            } while (startNumber <= 10);

            Console.WriteLine("DoWhile Result :: {0}",result);
        }

        public void TestBreakContinue() {
            int result = 0;

            for (int i = 0; i < 100; i++) {
                if (i > 10) {
                    break;
                }

                if ((i%2) == 0) {
                    continue;
                }

                result += i;
            }

            Console.WriteLine("BreakContinue Result :: {0}",result);
        }

        public void TestGoTo() {
            int result = 0;
            for (int i = 1; i <= 10; i++) {
                result += i;

                if (i == 5) {
                    goto Jump;
                }
            }

            Console.WriteLine("GoTo 1~10 Result :: {0}", result);
        Jump:
            Console.WriteLine("GoTo Jump :: {0}", result);
        }
    }

    public class Study8 {
        public void TestException() {
            this.DoException("123");
            this.DoException(null);
            this.DoException("일이삼");
        }

        private void DoException(string data) {
			try
			{
				int number = Int32.Parse(data);
			}
			catch (ArgumentNullException e)
			{
				Console.WriteLine("ArgumentNullException :: {0}", e.Message);
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception :: {0}", e.Message);
			}
        }

        public void TestThrow() {
            try {
                this.DoThrow("Hello??");
                this.DoThrow(null);
            } catch (ArgumentNullException e) {
                Console.WriteLine("ArgumentNullException :: {0}", e.Message);
            }
        }

        private void DoThrow(string message) {
            if (message == null) {
                throw new ArgumentNullException("message");
            }

            Console.WriteLine(message);
        }
    }

    public class Study9 {
        public void TestArray() {
            int[] array1D = { 1, 2, 3, 4, 5 };

            Console.WriteLine("Rank::{0}, Length::{1}", array1D.Rank, array1D.Length);

            Console.WriteLine("<<< Start >>>");

            foreach (int value in array1D) {
                Console.WriteLine(value);
            }

            Console.WriteLine("<<< For Start >>>");

            for (int i = 0; i < array1D.Length; i++) {
                Console.WriteLine("array[{0}] :: {1}", i, array1D[i]);
            }
        }

        public void TestArray2() {
            string[,] array2D = new string[5, 8];

            for (int i = 0; i < array2D.GetLength(0); i++) {
                for (int j = 0; j < array2D.GetLength(1); j++) {
                    array2D[i, j] = String.Format("{0}-{1}", i, j);
                }
            }

            for (int i = 0; i < array2D.GetLength(0); i++) {
                for (int j = 0; j < array2D.GetLength(1); j++) {
                    Console.Write("({0}) ", array2D[i, j]);
                }
                Console.WriteLine();
            }
        }

        public void TestJaggedArray() {
            string[][] arrayJagged = new string[3][];
            arrayJagged[0] = new string[5];
            arrayJagged[1] = new string[2];
            arrayJagged[2] = new string[3];

            for (int i = 0; i < arrayJagged.GetLength(0); i++) {
                for (int j = 0; j < arrayJagged[i].Length; j++) {
                    arrayJagged[i][j] = String.Format("{0}-{1}", i, j);
                }
            }

			for (int i = 0; i < arrayJagged.GetLength(0); i++)
			{
				for (int j = 0; j < arrayJagged[i].Length; j++)
				{
					Console.Write("({0}) ", arrayJagged[i][j]);
				}
				Console.WriteLine();
			}
        }

        public void TestArrayCopy () {
            Stopwatch sw1 = Stopwatch.StartNew();

            for (int i = 0; i < 10000000; i++) {
                int[] source = new int[100];
                int[] target = new int[100];

                Buffer.BlockCopy(source, 0, target, 0, 100);
            }
            sw1.Stop();


            Stopwatch sw2 = new Stopwatch();
            for (int i = 0; i < 10000000; i++) {
				int[] source = new int[100];
				int[] target = new int[100];

                Array.Copy(source, 0, target, 0, 100);
            }
            sw2.Stop();

            Console.WriteLine("Buffer.BlockCopy :: {0} ms",sw1.ElapsedMilliseconds);
            Console.WriteLine("Array.Copy :: {0} ms", sw2.ElapsedMilliseconds);
        }

        public void TestArraySort() {
            int[] array = new int[] { 3, 7, 1, 2, 5, 4, 6, 8, 9 };

            Console.WriteLine("<<< Start >>>");

            foreach(var value in array) {
                Console.Write("{0} ", value);
            }

            Console.WriteLine("<<< Sort >>>");

            Array.Sort(array);
            foreach (var item in array) {
                Console.Write("{0} ", item);
            }

			Console.WriteLine("<<< Revers >>>");
            Array.Reverse(array);
			foreach (var item in array)
			{
				Console.Write("{0} ", item);
			}
            Console.WriteLine();
        }
    }

    public class Study10 {

        public void TestFormat() {
            int number1 = 1;
            int number2 = 123;

            Console.WriteLine("number 1 :: |{0,5}|, number2 : {1:0000#}", number1, number2);
            Console.WriteLine("number 1 :: |{0,-5}|, number2 : {1:0000#}", number1, number2);

            int number3 = 12345;
            Console.WriteLine("number 3 :: {{ {0} }}", number3);
        }

        public void TestComa() {
            int number1 = 12345;
            int number2 = -12345;

            Console.WriteLine("number 1 :: {0:#,#;(#,#)}", number1);
            Console.WriteLine("number 2 :: {0}", number2.ToString("#,#(#,#)"));
        }

        public void TestFullZero() {
            int number1 = 1;
            int number2 = 123;
            int number3 = 1234;

            Console.WriteLine("number 1 :: {0:000##}", number1);
            Console.WriteLine("number 2 :: {0:0000#}", number2);
            Console.WriteLine("number 3 :: {0}", number3.ToString("00000"));
        }

        public void TestAbbreviation() {
            string systemFolder1 = "C:\\Windows\\System32";
            string systemFolder2 = @"C:\Windows\System32";

            Console.WriteLine("systemFolder 1 :: {0}", systemFolder1);
            Console.WriteLine("systemFolder 2 :: {0}", systemFolder2);
        }

        public void TestStringBuilder() {
            string text1 = "";

            Stopwatch sw1 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++) {
                text1 += i.ToString();
            }
            sw1.Stop();

            StringBuilder builder = new StringBuilder();

            Stopwatch sw2 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++) {
                builder.Append(i.ToString());
            }

            sw2.Stop();

            Console.WriteLine("String :: {0} ms", sw1.ElapsedMilliseconds);
            Console.WriteLine("StringBuilder :: {0} ms", sw2.ElapsedMilliseconds);
        }
    }
}
